import _ from "lodash";


class base {
    constructor() {
        this.table = "anonym";
    }

    get(params) {
        return this.callSocket(this.table, params).then(function ({result, data, error}) {
            if (result) {
                return data
            } else {
                console.log(error);
                return null;
            }
        });
    }
    getOne(params) {
        return this.callSocket(this.table, params).then(function ({result, data, error}) {
            if (result) {
                return data[0]
            } else {
                console.log(error);
                return null;
            }
        });
    }

    post(params) {
        return this.callSocket(this.table, params, 'post').then(function ({result, data, error}) {
            if (result) {
                return _.head(data)
            } else {
                console.log(error);
                return null;
            }
        });
    }

    put(params) {
        return this.callSocket(this.table, params, 'put').then(function ({result, data, error}) {
            if (result) {
                return _.head(data)
            } else {
                console.log(error);
                return null;
            }
        });
    }

    delete(params) {
        return this.callSocket(this.table, params, 'delete').then(function ({result, data, error}) {
            if (result) {
                return true;
            } else {
                console.log(error);
                return false;
            }
        });
    }

    inObject(array, field) {
        return _.has(array, field);
    }

    async callSocket(table, params, type = 'get') {
        let unique = `${_.toUpper(type)}_${_.toUpper(table)}_${_.now()}`;

        await window.socket.emit(type, {table: table, unique: unique, params: params});
        return await new Promise(resolve => {
            const interval = setInterval(() => {
                if (utils.inObject(window.query_store, unique)) {
                    let rs = window.query_store[unique];
                    resolve(rs);
                    clearInterval(interval);
                }
            }, 2);
        });
    }
}
$(function() {
    // Initialize variables
    window.query_store = {};
    const socket = window.socket = io(`ws://localhost:8081`);

    socket.on('get_response', function ({unique, result, data, error}) {
        window.query_store[unique] = {unique, result, data, error};
    });
    socket.on('post_response', function ({unique, result, data, error}) {
        window.query_store[unique] = {unique, result, data, error};
    });
    socket.on('put_response', function ({unique, result, data, error}) {
        window.query_store[unique] = {unique, result, data, error};
    });
    socket.on('delete_response', function ({unique, result, data, error}) {
        window.query_store[unique] = {unique, result, data, error};
    });
    socket.on('disconnect', function () {
        console.log('you have been disconnected');
    });
    socket.on('reconnect', function () {
        console.log('you have been reconnected');

    });
    socket.on('reconnect_error', function () {
        console.log('attempt to reconnect has failed');
    });
});
