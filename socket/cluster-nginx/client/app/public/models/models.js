class news extends base {
    constructor() {
        super();
        this.table = 'news';
    }
}
class order extends base {
    constructor() {
        super();
        this.table = 'order';
    }
}
let models = {
    news: new news(),
    order: new order()
};