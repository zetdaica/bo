const src = [
    "libs/socket.io.min.js",
    "https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js",
    "models/base.js",
    "libs/utils.js",
    "models/models.js",
    "script.js",
];

src.map(async (src) => {
    await console.log(src);

    await new Promise(resolve => {
        loadScript(src, async function () {
            await console.log(src, "load done");
            resolve(true);
        });
    });
});

function loadScript(url, callback) {
    // Adding the script tag to the head as suggested before
    const head = document.head;
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

    // Fire the loading
    head.appendChild(script);
}