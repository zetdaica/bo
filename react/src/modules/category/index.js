import React from 'react';

import Main from './main'
import Detail from './detail'



class Category extends React.Component {

    state = {
        home: false,
        detail: false,
        model: 'category'
    };

    handleChangePage = (page, data = {}) => {
        this.setState({
            home: page === 'main',
            edit: page === 'edit',
            add: page === 'add',
            data : data
        })
    };

    render() {
        const {edit, data, add, model} = this.state;
        const {showForm} = this.props;

        if (showForm) {
            return <Detail model={model} add={add} data={data} handleChangePage={this.handleChangePage}/>
        }
        if (edit) {
            return <Detail model={model} add={add} data={data} handleChangePage={this.handleChangePage}/>
        }else if(add) {
            return <Detail model={model} add={add} data={data} handleChangePage={this.handleChangePage}/>
        } else {
            return <Main model={model} data={data} handleChangePage={this.handleChangePage}/>
        }
    }
}

export default Category;
