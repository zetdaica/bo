import React, {Component} from 'react';
import {Form, Input} from 'formsy-semantic-ui-react';
import {Segment, Feed, Icon, Message,Item} from 'semantic-ui-react';
import _ from 'lodash';
import {io} from "socket.io-client";
import utils from  '../../../helpers/utils' ;
import models from  '../../../models/models' ;
const adminStyle = {
    textAlign: 'right'
}
const socket = io(`ws://${process.env.REACT_APP_HOST}`);
const project = process.env.REACT_APP_PROJECT;

class Detail extends Component {
    constructor(props) {
        super(props)
        this.el = React.createRef()
    }
    componentDidUpdate = () => {
        this.scrollToBottom()
    };
    scrollToBottom = () => {
        if (!_.isEmpty(this.el.current)){
            this.el.current.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' })
        }
    };
    state = {
        username: '',
        events: []
    };
    refs = {}
    componentDidMount() {
        socket.on('chating',  (data) => {
            let {events, username} = this.state
            this.setState({
                events: _.concat(events, {
                    project: project,
                    name:username,
                    sender: username,
                    message: data.message,
                    receiver: 'admin'
                })
            })

        });
        let userStore = utils.getCookie('chat_name');
        if (userStore) {
            this.getChatHistory(userStore)
        }
    }
    getChatHistory = async (userStore) => {
        const chats = await models.chat.get({name:userStore});
        if (!_.isEmpty(chats)) {
            this.setState({
                username: userStore,
                events: chats
            })
        }
    };
    sendMessage = (message) => {
        let {username} = this.state;
        socket.emit('chating', {
            project: project,
            name:username,
            sender: username,
            message: message,
            receiver: 'admin',
            seen: 0
        });
    };
    onValidSubmit = (data) => {
        const {username, events} = this.state;
        if (_.isEmpty(username)) {
           this.setState({
               username: data.username
           }, () => {
               utils.storeCookie('chat_name', data.username, 60)
           })
        } else {
            this.setState({
                events: _.concat(events, {
                    project: project,
                    name:username,
                    sender: username,
                    message: data.message,
                    receiver: 'admin'
                })
            });
            this.sendMessage(data.message);
            this.resetForm();
        }
    };
    resetForm = () => {
        this.refs.form.reset();
    };
    render() {
        const {username, events} = this.state;
        return (
            <div>
                <Segment>
                    <Item.Group className="chat-over-frame">
                        {!_.isEmpty(events) && events.map((row, index) => (
                            <Item key={index}>

                                <Item.Content style={row.sender === 'admin' ? adminStyle : {}}>
                                    {row.sender !== 'admin' ? (<Icon color="teal" size="big" name="user circle"/>) : ''}

                                    <Message success={(row.sender === 'admin')} compact className="chat-border">
                                        <Item.Meta>
                                            {row.sender}
                                        </Item.Meta>

                                        <Item.Description>
                                            {row.message}
                                        </Item.Description>
                                    </Message>
                                    {row.sender === 'admin' ? (<Icon color="yellow" size="big" name="smile"/>) : ''}

                                </Item.Content>

                            </Item>
                        ))}
                        <div ref={this.el} />
                    </Item.Group>
                </Segment>
                <Segment>
                    <Form ref="form" onSubmit={this.onValidSubmit}>
                        {
                            _.isEmpty(username) && (
                                <Form.Field
                                    control={Input}
                                    label='username'
                                    name="username"
                                    placeholder='Type Username'
                                    width={14}
                                />
                            )
                        }
                        {
                            !_.isEmpty(username) && (
                                <Form.Field
                                    control={Input}
                                    label={username + ' on the Chat'}
                                    name="message"
                                    placeholder='message'
                                    width={14}
                                />
                            )
                        }

                    </Form>
                </Segment>
            </div>

        )
    }
}

export default Detail