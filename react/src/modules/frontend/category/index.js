import {useParams} from "react-router-dom";
import Main from './main'
function Category(props) {
    const {id} = useParams();

    return (
        <div>
            <Main category={props.category} categoryId={id}/>
        </div>
    )
}

export default Category