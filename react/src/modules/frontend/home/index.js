import React, {useEffect, useState} from 'react'
import {
    Segment,
    Grid,
    Image, Card, Divider, Button, Label, Item
} from 'semantic-ui-react';
import models from "../../../models/models";
import _ from 'lodash'
import constant from "../../../helpers/constant";
import lang from "../../../helpers/lang";
import utils from "../../../helpers/utils";
import {Table} from "semantic-ui-react/dist/commonjs/collections/Table";
function FixedMenuLayout(props) {
    const [hotProduct, setHotProduct] = useState([]);
    const [saleProduct, setSaleProduct] = useState([]);
    useEffect(() => {
        document.title = "Home";
        init();
    }, []);
    const init = () => {
        models.product.get({type:'hot', status: constant.VAR.ACTIVE}).then((res) => {
            setHotProduct(res)
        });
        models.product.get({type:'sale', status: constant.VAR.ACTIVE}).then((res) => {
            setSaleProduct(res)
        });
    };
    return (
        <div>
            <Segment raised>
                <Divider horizontal>
                    <Label as='a' color='orange' ribbon>
                        {lang('hot_product')}
                    </Label>
                </Divider>
            </Segment>
            <Grid doubling columns={4}>
                {
                    !_.isEmpty(hotProduct) && hotProduct.map((row, index) => (
                        <Grid.Column key={index}>
                            <Card>
                                <Image width="200" src={utils.parseImage(row.image)} size='small' wrapped />
                                <Card.Content>
                                    <Card.Header>{row.title}</Card.Header>
                                    <Card.Description color="red">
                                        <Label color="teal">
                                            {row.price}/Thùng/24Chai
                                        </Label>
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                        </Grid.Column>

                    ))
                }
            </Grid>

            <Segment raised>
                <Divider horizontal>
                    <Label as='a' color='yellow' ribbon>
                        {lang('sale_product')}
                    </Label>
                </Divider>
            </Segment>
            <Item.Group relaxed>
                {
                    !_.isEmpty(saleProduct) && saleProduct.map((row, index) => (
                        <Item key={index}>
                            <Image width="200" src={utils.parseImage(row.image)} size='small' wrapped />

                            <Item.Content>
                                <Item.Header>{row.title}</Item.Header>
                                <Item.Description>
                                    <Label color="yellow">
                                        {row.price}/Thùng/24Chai
                                    </Label>
                                </Item.Description>
                                <Item.Extra>
                                    <Button floated='right'>More</Button>
                                </Item.Extra>
                            </Item.Content>
                        </Item>
                    ))
                }

            </Item.Group>

        </div>
    )
}

export default FixedMenuLayout