import React from 'react';

import {Card, Grid, Segment, Image, Icon, Button, Divider, Label} from 'semantic-ui-react'
import _ from 'lodash';
import file from '../../helpers/file';
import DropzoneComponent from "../../helpers/ui/DropzoneComponent";
import {Form, Input} from "formsy-semantic-ui-react";
import utils from '../../helpers/utils';


const deleteStyle = {
    opacity: 0.7,
    backgroundColor: "lightyellow"
}
class Content extends React.Component{
    componentDidMount() {
        document.title = 'Admin File manager';
        this.getList();
    }

    state = {
        list : {},
        currentDir: '',
        type : 'upload'
    };
    getList = async (dir = '', path = '') => {
        const rs = await file.getDir(dir);

        if (rs) {
            this.setState({
                dir: rs.dir,
                files: rs.files,
                currentDir: path
            }, () => {
                // console.log(this.state)
            })
        }
    };

    onHasFile = async (files) => {
        const {currentDir} = this.state;
        if (!_.isEmpty(files)) {
            const rs = await file.uploadFile(files, currentDir);
            if(rs){
                await this.getList(currentDir, currentDir)
            }
        }
    };
    handleType = (t) => {
        const {type} = this.state;
        if (t !== type) {
            this.setState({
                type: t
            })
        }
    };

    createNewDir = async (data) => {
        const {currentDir} = this.state;
        if (!_.isEmpty(data.newDir)) {
            const rs = await file.createDir(`${currentDir}/${data.newDir}`);
            if(rs){
                await this.getList(currentDir, currentDir)
            }
        }
    };
    handleReturn = () => {
        const {currentDir} = this.state;
        const lastIndex = _.lastIndexOf(currentDir, '/');
        if (lastIndex >= 0) {
            const path = currentDir.slice(0, lastIndex);
            this.getList(path, path);
        }
    };

    handleDir = async (name, path) => {
        const {type, currentDir} = this.state;
        if (type === 'remove') {
            const rs = await file.removeDir(path);
            if (rs) {
                await this.getList(currentDir, currentDir)
            }
        } else {
            this.setState({
                currentDir: path
            }, () => {
                this.getList(path, path)
            });
        }
    };
    handleFile = async (name) => {
        const {type, currentDir} = this.state;
        const {onHasClick} = this.props;
        if (type === 'remove') {
            const rs = await file.removeFile(`${currentDir}/${name}`);
            if (rs) {
                await this.getList(currentDir, currentDir)
            }
        } else {
            onHasClick(`${currentDir}/${name}`)
        }
    };

    render() {
        const {dir, files, currentDir, type} = this.state;
        const {action} = this.props;

        return (
            <div style={{padding: '50px'}}>
                {action && (
                    <Segment padded='very'>
                        <Button.Group>
                            <Button color={type === 'upload' ? 'teal' : 'grey'} onClick={() => this.handleType('upload')} icon='upload'/>
                            <Button color={type === 'create_dir' ? 'teal' : 'grey'} onClick={() => this.handleType('create_dir')} icon="folder outline"/>
                            <Button color={type === 'remove' ? 'red' : 'grey'} onClick={() => this.handleType('remove')} icon="remove"/>

                        </Button.Group>
                        <div style={{marginTop: '10px'}}>
                            {
                                type === 'upload' && (
                                    <DropzoneComponent
                                        onHasFile={this.onHasFile}
                                        placeholder='Drag "n" drop a file, or click to select file...'
                                        preview={false}
                                    />
                                )
                            }
                            {
                                type === 'create_dir' && (
                                    <div>
                                        <Form onSubmit={this.createNewDir}>
                                            <Input width={6} type='text' name="newDir" placeholder="Type New Folder" action>
                                                <input />
                                                <Button color="blue" type="submit" icon='save'/>
                                            </Input>
                                        </Form>
                                    </div>
                                )
                            }
                        </div>
                    </Segment>
                )}


            <Segment style={(type === 'remove') ? deleteStyle : {}} padded='very'>
                {!_.isEmpty(currentDir) && (
                    <div>
                        <Button color="orange" onClick={() => this.getList()} icon='home'/>
                        <Button color="green" onClick={() => this.handleReturn()} icon='reply'/>
                    </div>
                )}
                {!_.isEmpty(currentDir) && <h4><Icon name="folder open"/>{currentDir}</h4>}

                {(type === 'remove') && (<Label as="a" color='blue' ribbon >Warning: Remove State Click Item to remove it</Label>)}

                <Divider />
                <Grid columns={action ? 6: 4}>
                    <Grid.Row>
                    {!_.isEmpty(dir) && dir.map((d, i) => (
                        <Grid.Column onClick={() => this.handleDir(d.name, `${d.path}/${d.name}`)} key={i}>
                            <Icon name="folder" size="huge" color="yellow"/>
                            <span>{d.name}</span>
                        </Grid.Column>
                    ))}
                    </Grid.Row>
                    {!_.isEmpty(files) && files.map((f, i) => (
                        <Grid.Column key={i}>
                            <Card onClick={()=> this.handleFile(f)}>
                                <Image size='medium' src={`${utils.parseImage(currentDir)}/${f}`}/>
                                <Card.Content>
                                    <Card.Description>{f}</Card.Description>
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                    ))}
                </Grid>
            </Segment>
            </div>
        );
    }
}

export default Content;
