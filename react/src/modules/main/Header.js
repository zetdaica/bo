import React, {useEffect, useState} from 'react';

import {Menu, Container, Icon} from 'semantic-ui-react'
import auth from '../../auth'
import {useHistory} from 'react-router-dom';
import constant from "../../helpers/constant";

function Header() {
    const [username, setUsername] = useState('');
    const history = useHistory();
    const name = process.user ? process.user.username : 'anonymos';
    useEffect(() => {
        setUsername(name)
    },[name])
    const handleLogout = () => {
        auth.logOut();
        history.push(constant.ROUTES.AUTH.LOGIN)
    };
    return (
        <div className="header" style={{marginBottom: '50px'}}>
            <Menu color="teal" fixed='top' inverted>
                <Container>
                    <Menu.Item as='a' header>
                        Management Tool
                    </Menu.Item>
                    <Menu.Menu position='right'>
                        <Menu.Item as='a'><Icon name="user"/><b>{username}</b></Menu.Item>
                        <Menu.Item onClick={() => handleLogout()} as='a'><Icon name="log out"/></Menu.Item>

                    </Menu.Menu>
                </Container>
            </Menu>

        </div>
    );
}

export default Header;
