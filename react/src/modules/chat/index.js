import React from 'react';
import Main from './main'

class Chat extends React.Component {

    state = {
        model: 'chat'
    };

    render() {
        const {model} = this.state;

        return <Main model={model}/>
    }
}

export default Chat;
