import React from 'react';

import {Button, Item} from 'semantic-ui-react'
import models from '../../models/models'
import { Link } from "react-router-dom";
import constant from "../../helpers/constant";
import utils from "../../helpers/utils";

class Content extends React.Component{
    componentDidMount() {
        this.getList();
    }
    state = {
        list : {}
    };
    getList = () => {
        models.news.get().then((res) => {
            this.setState({
                list: res
            })
        })
    };

    render() {
        return (
            <div className="content">
                <h3>Dashboard is in process</h3>
                <Item.Group>
                    <Item>
                        <Item.Image size='small' src='https://react.semantic-ui.com/images/wireframe/image.png' />

                        {/*<Item.Content>*/}
                            {/*<Item.Header as='a'>CATEGORY</Item.Header>*/}
                            {/*<Item.Description>*/}
                                {/*<Button as={Link} size='massive'color='blue' to={constant.ROUTES.BE.CATEGORY}>List</Button>*/}
                                {/*<Button onClick={() => utils.changLanguage()} size='massive'color='olive'>Add New Item</Button>*/}
                            {/*</Item.Description>*/}
                        {/*</Item.Content>*/}
                    </Item>

                    <Item>
                        <Item.Image size='small' src='https://react.semantic-ui.com/images/wireframe/image.png' />

                        {/*<Item.Content>*/}
                            {/*<Item.Header as='a'>PRODUCT</Item.Header>*/}
                            {/*<Item.Description>*/}
                                {/*<Button as={Link} size='massive'color='blue' to={constant.ROUTES.BE.PRODUCT}>List</Button>*/}
                                {/*<Button onClick={() => utils.changLanguage()} size='massive'color='olive'>Add New Item</Button>*/}
                            {/*</Item.Description>*/}
                        {/*</Item.Content>*/}
                    </Item>

                    <Item>
                        <Item.Image size='small' src='https://react.semantic-ui.com/images/wireframe/image.png' />

                        {/*<Item.Content>*/}
                            {/*<Item.Header as='a'>BANNER</Item.Header>*/}
                            {/*<Item.Description>*/}
                                {/*<Button as={Link} size='massive'color='blue' to={constant.ROUTES.BE.BANNER}>List</Button>*/}
                                {/*<Button onClick={() => utils.changLanguage()} size='massive'color='olive'>Add New Item</Button>*/}
                            {/*</Item.Description>*/}
                        {/*</Item.Content>*/}
                    </Item>
                </Item.Group>

            </div>
        );
    }
}

export default Content;
