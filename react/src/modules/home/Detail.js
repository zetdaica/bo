import React, { Component } from 'react'
import {
    Button,

} from 'semantic-ui-react'
import {
    Form, Input, TextArea, Select
} from 'formsy-semantic-ui-react';
import models from '../../models/models';
import _ from 'lodash';


const options = [
    { key: 'm', text: 'Male', value: 'male' },
    { key: 'f', text: 'Female', value: 'female' },
    { key: 'o', text: 'Other', value: 'other' },
];

class FormExampleFieldControl extends Component {

    onValidSubmit = (data) => {
        const {handleChangePage, add} = this.props;

        console.log("submit", _.omit(data, '_id'));
        if (add) {
            models.news.post(data).then((res) => {
                if (!res) {
                    alert('Created failed')
                } else {
                    handleChangePage('home');
                }
            })
        } else {
            models.news.put({_id: data._id}, _.omit(data, '_id')).then((res) => {
                if (!res) {
                    alert('Update failed')
                } else {
                    handleChangePage('home');
                }
            })
        }
    }

    render() {
        const {handleChangePage, data} = this.props;

        return (
            <div>
                {!_.isEmpty(data) && (
                    <h2>{data._id}</h2>
                )}
                <Form onSubmit={this.onValidSubmit}>
                    <Form.Group widths='equal' >
                        <Form.Field
                            control={Input}
                            label='Name'
                            name="name"

                            placeholder='Name'
                            value={data.name}
                        />
                        <Form.Field
                            control={Input}
                            label='Test'
                            name="test"
                            placeholder='Test'
                            value={data.test}
                        />
                        <Form.Field
                            control={Select}
                            name="gender"
                            label='Gender'
                            options={options}
                            placeholder='Gender'
                        />
                    </Form.Group>

                    <Form.Field
                        control={TextArea}
                        name="content"
                        label='About'
                        placeholder='Tell us more about you...'
                    />

                    <Button color='teal' onClick={() => handleChangePage('home')}>Back</Button>
                    <Button color='grey' type="submit">Submit</Button>
                </Form>
            </div>

        )
    }
}

export default FormExampleFieldControl