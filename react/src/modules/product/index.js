import React from 'react';

import Main from './main'
import Detail from './detail'
import models from "../../models/models";


class Index extends React.Component {

    componentDidMount() {
        this.init();
    }
    state = {
        home: false,
        detail: false,
        model: 'product',
        category : {},
        data: {}

    };
    init = () => {
        models.category.get().then((res) => {
            this.setState({
                category: res
            })
        })
    };
    handleChangePage = (page, data = {}) => {
        this.setState({
            home: page === 'main',
            edit: page === 'edit',
            add: page === 'add',
            data : data
        })
    };

    render() {
        const {edit, data, add, model, category} = this.state;

        if (edit) {
            return <Detail model={model}
                           category={category}
                           add={add}
                           data={data}
                           handleChangePage={this.handleChangePage}
            />
        }else if(add) {
            return <Detail model={model}
                           category={category}
                           add={add}
                           data={data}
                           handleChangePage={this.handleChangePage}
            />
        } else {
            return <Main model={model}
                         category={category}
                         data={data}
                         handleChangePage={this.handleChangePage}
            />
        }
    }
}

export default Index;
