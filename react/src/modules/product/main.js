import React from 'react';

import {Table, Button, Label, Image} from 'semantic-ui-react'
import models from '../../models/models'
import _ from 'lodash';
import utils from '../../helpers/utils';
import lang from "../../helpers/lang";
const color = {
    hot: "red",
    sale: "yellow"
}
class Content extends React.Component{
    componentDidMount() {
        document.title = 'Admin Product';
        this.getList();
    }

    state = {
        list : {},
    };
    getList = () => {
        const {model} = this.props;
        let base = this;
        models.category.get().then(function (categories) {
            models[model].get().then((res) => {
                const map = res.map((r) => {
                    let rs = utils.findInArray(categories, {_id: r.category});
                    return !_.isEmpty(rs) ? {...r, categoryName: rs.title} : r;
                });
                base.setState({
                    list: map
                })
            });
        })

    };
    deleteRow = (e, index) => {
        const {model} = this.props;

        models[model].delete({_id: index}).then((res) => {
            this.getList();
        })
    };
    toggleRow = (data) => {
        const {model} = this.props;

        models[model].put({_id: data._id}, {status: this.revertStatus(data.status)}).then((res) => {
            if (!res) {
                alert('toggleRow failed')
            } else {
                this.getList();
            }
        })
    };
    revertStatus = (status) => {
        return status === '1' ? '0' : '1';
    }

    render() {
        const {list} = this.state;
        const {handleChangePage} = this.props;
        return (
            <div className="content">
                <h2>{lang('product')}</h2>
                <Button icon="add" color='pink' onClick={() => handleChangePage('add')}/>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Title</Table.HeaderCell>
                            <Table.HeaderCell>Image</Table.HeaderCell>
                            <Table.HeaderCell>Category</Table.HeaderCell>
                            <Table.HeaderCell>Price</Table.HeaderCell>
                            <Table.HeaderCell>Type</Table.HeaderCell>
                            <Table.HeaderCell>Status</Table.HeaderCell>
                            <Table.HeaderCell>Action</Table.HeaderCell>

                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {
                            !_.isEmpty(list) && list.map((row, index) => {
                                return (
                                    <Table.Row negative={!(row.status === '1')} key={index}>
                                        <Table.Cell>{row.title}</Table.Cell>
                                        <Table.Cell>
                                            <Image width="200" src={utils.parseImage(row.image)} size='small' wrapped />
                                        </Table.Cell>
                                        <Table.Cell>
                                            <Label color='teal' horizontal>
                                                {row.categoryName}
                                            </Label>

                                        </Table.Cell>
                                        <Table.Cell>{row.price}</Table.Cell>
                                        <Table.Cell>
                                            <Label as='span' color={color[row.type]} tag>
                                                {_.toUpper(row.type)}
                                            </Label>

                                        </Table.Cell>
                                        <Table.Cell>
                                            {
                                                row.status === '1' ? (
                                                    <Label as='span' color='green' tag>
                                                        Active
                                                    </Label>
                                                    ) : (
                                                    <Label as='span' tag>
                                                        Inactive
                                                    </Label>
                                                )
                                            }
                                        </Table.Cell>

                                        <Table.Cell collapsing textAlign='right'>
                                            <div>
                                                <Button onClick={(e) => this.toggleRow(row)} color={row.status === '1' ? 'green' : 'yellow'} icon="eye"/>
                                                <Button color='blue'>View</Button>
                                                <Button color='teal' onClick={() => handleChangePage('edit', row)}>Edit</Button>
                                                <Button onClick={(e) => this.deleteRow(e, row._id)} color='red'>Delete</Button>
                                            </div>
                                        </Table.Cell>
                                    </Table.Row>
                                )
                            })
                        }
                    </Table.Body>
                </Table>
            </div>
        );
    }
}

export default Content;
