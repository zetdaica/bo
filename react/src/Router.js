import React, {useEffect, useState} from 'react';

import {BrowserRouter as Router, Route} from "react-router-dom";
import CONSTANT from './helpers/constant'
import {Container, Grid} from "semantic-ui-react";
import {Footer, Header, Menu} from "./modules/main";
import {FooterFE, HeaderFE, Sidebar} from "./modules/frontend/main";

// FRONTEND
import FeHome from './modules/frontend/home';
import FeCategory from './modules/frontend/category';
import FeSingle from './modules/frontend/single';
import FeChat from './modules/frontend/chat';

// BACKEND
import BeHome from "./modules/home";
import BeCategory from "./modules/category";
import BeProduct from "./modules/product";
import BeBanner from "./modules/banner";
import BeBrand from "./modules/brand";
import BeNews from "./modules/news";
import BeSingle from "./modules/single";
import BeFileManager from "./modules/filemanager";
import BeUser from "./modules/user";
import BeLogin from "./auth/LoginForm";
import BeChat from "./modules/chat";

import models from "./models/models";
import auth from "./auth";
import {Redirect} from 'react-router-dom';

function RouterComponent() {

    const [category, setCategory] = useState([]);

    useEffect(() => {
        init();
    }, []);
    const init = () => {
        models.category.getActive().then((res) => {
            setCategory(res)
        });
    };
    return (
        <Router>
            <Route
                exact
                path={CONSTANT.ROUTES.AUTH.LOGIN}
                component={BeLogin}
            />
            <Route
                path={`${CONSTANT.ROUTES.FE.CHAT}`}
                exact
                render={props => <FeChat {...props} category={category}/>}
            />
            <Route path={CONSTANT.ROUTES.BE.HOME}
                   render={(props) => {
                       if (!auth.isAuthenticated()) {
                           return <Redirect to={CONSTANT.ROUTES.AUTH.LOGIN} />
                       }
                       auth.setUser();
                       return (
                           <div>
                               <Header/>
                               <Grid columns={2}>

                                   <Grid.Row>
                                       <Grid.Column width={2}>
                                           <div style={{padding: '15px'}}>
                                               <Menu/>
                                           </div>
                                       </Grid.Column>
                                       <Grid.Column width={14}>
                                           <div style={{padding: '15px'}}>
                                               <Route path={CONSTANT.ROUTES.BE.HOME} exact
                                                      render={props => <BeHome {...props}/>}

                                               />
                                               <Route path={CONSTANT.ROUTES.BE.CATEGORY}
                                                      exact
                                                      component={BeCategory}
                                                      showFrom={false}/>
                                               <Route path={CONSTANT.ROUTES.BE.PRODUCT}
                                                      exact
                                                      component={BeProduct}/>
                                               <Route path={CONSTANT.ROUTES.BE.BANNER}
                                                      exact
                                                      component={BeBanner}/>
                                               <Route path={CONSTANT.ROUTES.BE.BRAND}
                                                      exact
                                                      component={BeBrand}/>
                                               <Route path={CONSTANT.ROUTES.BE.NEWS}
                                                      exact
                                                      component={BeNews}/>
                                               <Route path={CONSTANT.ROUTES.BE.FILE_MANAGER}
                                                      exact
                                                      component={BeFileManager}/>
                                               <Route path={CONSTANT.ROUTES.BE.USER}
                                                      exact
                                                      component={BeUser}/>
                                               <Route path={`${CONSTANT.ROUTES.BE.SINGLE}/:slug`}
                                                      exact
                                                      render={props => <BeSingle {...props}/>}
                                               />
                                               <Route path={`${CONSTANT.ROUTES.BE.CHAT}`}
                                                      exact
                                                      render={props => <BeChat {...props}/>}
                                               />
                                           </div>
                                       </Grid.Column>
                                   </Grid.Row>
                               </Grid>
                               <Footer/>
                           </div>
                       )
                   }}
            >
            </Route>

            <Route path="/"
                   exact
                   render={(props) => (
                       <div className="bg-img-base">
                           <HeaderFE category={category}/>

                           <Container style={{marginTop: '4rem'}}>
                               <Grid divided>
                                   <Grid.Column width={4}>
                                       <Sidebar category={category}/>
                                   </Grid.Column>
                                   <Grid.Column width={12}>
                                       <Route
                                           path={CONSTANT.ROUTES.FE.HOME}
                                           exact
                                           render={props => <FeHome {...props} category={category}/>}
                                       />
                                   </Grid.Column>
                               </Grid>
                           </Container>

                           <FooterFE/>
                       </div>
                   )}
            >
            </Route>
            <Route path="/fe"
                   render={(props) => (
                       <div className="bg-img-base">
                           <HeaderFE category={category}/>

                           <Container style={{marginTop: '4rem'}}>
                               <Grid divided>
                                   <Grid.Column width={4}>
                                       <Sidebar category={category}/>
                                   </Grid.Column>
                                   <Grid.Column width={12}>
                                       <Route
                                           path={CONSTANT.ROUTES.FE.HOME}
                                           exact
                                           render={props => <FeHome {...props} category={category}/>}
                                       />
                                       <Route
                                           path={`${CONSTANT.ROUTES.FE.CATEGORY}/:slug/:id`}
                                           exact
                                           render={props => <FeCategory {...props} category={category}/>}
                                       />
                                       <Route
                                           path={`${CONSTANT.ROUTES.FE.SINGLE}/:slug`}
                                           exact
                                           render={props => <FeSingle {...props} category={category}/>}
                                       />
                                   </Grid.Column>
                               </Grid>
                           </Container>

                           <FooterFE/>
                       </div>
                   )}
            >
            </Route>

        </Router>
    );
}

export default RouterComponent
