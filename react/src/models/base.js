import _ from 'lodash';
import constant from "../helpers/constant";
const project = process.env.REACT_APP_PROJECT;

class Base {
    constructor() {
        this.table = "anonym";
    }

    get(params, sort = {}) {
        return this.callSocket({params: params, type: 'get', sort: sort}).then(function ({result, data, error}) {
            if (result) {
                return data
            } else {
                console.log(error);
                return null;
            }
        });
    }
    async getOne(params) {
        const rs = await this.get(params);
        return _.isEmpty(rs) ? {} : rs[0]
    }
    post(params) {
        return this.callSocket({params: params, type: 'post'}).then(function ({result, data, error}) {
            if (result) {
                return _.head(data)
            } else {
                console.log(error);
                return null;
            }
        });
    }

    put(where, params) {
        return this.callSocket({params: params, type: 'put', where: where}).then(function ({result, data, error}) {
            if (result) {
                return true
            } else {
                console.log(error);
                return false;
            }
        });
    }

    delete(params) {
        return this.callSocket({params: params, type: 'delete'}).then(function ({result, data, error}) {
            if (result) {
                return true;
            } else {
                console.log(error);
                return false;
            }
        });
    }

    inObject(array, field) {
        return _.has(array, field);
    }

    getActive() {
        return this.get({status: constant.VAR.ACTIVE})
    }
    async callSocket({params, type, where, sort}) {
        let unique = `${_.toUpper(project)}_${_.toUpper(this.table)}_${_.uniqueId(type)}`;

        await process.socket.emit(type, {
            project: project,
            table: this.table,
            unique: unique,
            params: params,
            where: where,
            sort: sort
        });
        return await new Promise(resolve => {
            let i = 300;
            const interval = setInterval(() => {
                if (this.inObject(process.query_store, unique)) {
                    let rs = process.query_store[unique];
                    resolve(rs);
                    clearInterval(interval);
                }
                i--;
                if (i <= 0) {
                    clearInterval(interval);
                    resolve({result: false, error: "max interval"})
                }
            }, 2);
        });
    }
}
export default Base;

