import Base from './base';
class News extends Base {
    constructor() {
        super();
        this.table = 'news';

    }
}
class Order extends Base {
    constructor() {
        super();
        this.table = 'order';
    }
}
class Category extends Base {
    constructor() {
        super();
        this.table = 'category';
    }
}
class Banner extends Base {
    constructor() {
        super();
        this.table = 'banner';
    }
}
class Product extends Base {
    constructor() {
        super();
        this.table = 'product';
    }
}
class Brand extends Base {
    constructor() {
        super();
        this.table = 'brand';
    }
}
class Single extends Base {
    constructor() {
        super();
        this.table = 'single';
    }
}
class User extends Base {
    constructor() {
        super();
        this.table = 'user';
    }
}
class Chat extends Base {
    constructor() {
        super();
        this.table = 'chat';
    }
}
const models = {
    news: new News(),
    order: new Order(),
    category: new Category(),
    product: new Product(),
    brand: new Brand(),
    banner: new Banner(),
    single: new Single(),
    user: new User(),
    chat: new Chat(),

};
export default models;