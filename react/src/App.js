import './App.css';
import {io} from 'socket.io-client';
import React from 'react';
import RouterComponent from './Router';
// console.log(process.env.REACT_APP_HOST);
const socket = process.socket = io(`ws://${process.env.REACT_APP_HOST}`);
process.query_store = {};

socket.on('connect', () => {
    console.log('connected to the socket');
});
socket.on('get_response', function ({unique, result, data, error}) {
    process.query_store[unique] = {unique, result, data, error};
});
socket.on('post_response', function ({unique, result, data, error}) {
    process.query_store[unique] = {unique, result, data, error};
});
socket.on('put_response', function ({unique, result, data, error}) {
    process.query_store[unique] = {unique, result, data, error};
});
socket.on('delete_response', function ({unique, result, data, error}) {
    process.query_store[unique] = {unique, result, data, error};
});
socket.on('file_response', function ({unique, result, data, error}) {
    process.query_store[unique] = {unique, result, data, error};
});
socket.on('disconnect', function () {
    console.log('you have been disconnected');
});
socket.on('reconnect', function () {
    console.log('you have been reconnected');

});
socket.on('reconnect_error', function () {
    console.log('attempt to reconnect has failed');
});

class App extends React.Component {
    render() {
        return (
            <div className="App">
                <RouterComponent/>
            </div>
        );
    }
}

export default App;
