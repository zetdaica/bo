import React, {useEffect, useState} from 'react';
import Main from '../../modules/filemanager/main';
import {Modal, Button, Image} from 'semantic-ui-react';
import _ from 'lodash';

function FileManager(props) {
    const [open, setOpen] = useState(false);
    const {image} = props;
    useEffect(() => () => {
        // Make sure to revoke the data uris to avoid memory leaks
    }, []);
    const onHasClick = (dir) => {
        setOpen(false);
        return props.onHasClick(dir)
    };

    return (
        <>
            <Modal
                onClose={() => setOpen(false)}
                onOpen={() => setOpen(true)}
                open={open}
                trigger={<Button type="button">Choose Image</Button>}
            >
                <Modal.Header>Select a Photo</Modal.Header>
                <Modal.Content image>
                    <Main action={props.action} onHasClick={onHasClick}/>
                </Modal.Content>
                <Modal.Actions>
                    <Button type="button" color='black' onClick={() => setOpen(false)}>
                        Nope
                    </Button>
                    <Button
                        type="button"
                        content="Yep, that's me"
                        labelPosition='right'
                        icon='checkmark'
                        onClick={() => setOpen(false)}
                        positive
                    />
                </Modal.Actions>
            </Modal>
            <div>
                {!_.isEmpty(image) && <Image src={`${window.location.origin}/uploads/${image}`} size='medium' wrapped />}
            </div>
        </>

    );
}

export default FileManager;