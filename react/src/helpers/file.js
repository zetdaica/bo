import _ from 'lodash';
import utils from './utils';
const type = {
    UPLOAD: 'upload',
    GET_DIR: 'get_dir',
    CREATE_DIR: 'create_dir',
    REMOVE_DIR: 'remove_dir',
    REMOVE_FILE: 'remove_file',

};
const project = process.env.REACT_APP_PROJECT;
console.log(project);
class File {
    async uploadFile(files, dir = '') {
        try {
            const nfiles = files.map(function (f) {
                return {
                    file: f,
                    name: f.name
                }
            })
            return this.callSocket({type: type.UPLOAD, files: nfiles, path: dir}).then(function ({result, data, error}) {
                if (result) {
                    return data
                } else {
                    console.log(error);
                    return null;
                }
            });
        } catch (e) {
            console.log(e)
            return null;
        }
    }
    getDir(dir){
        return this.callSocket({type: type.GET_DIR, path: dir}).then(function ({result, data, error}) {
            if (result) {
                return data
            } else {
                console.log(error);
                return null;
            }
        });
    }
    createDir(dir) {
        return this.callSocket({type: type.CREATE_DIR, path: dir}).then(function ({result, data, error}) {
            if (result) {
                return true
            } else {
                console.log(error);
                return false;
            }
        });
    }
    removeDir(dir) {
        return this.callSocket({type: type.REMOVE_DIR, path: dir}).then(function ({result, data, error}) {
            if (result) {
                return true
            } else {
                console.log(error);
                return false;
            }
        });
    }
    removeFile(dir) {
        return this.callSocket({type: type.REMOVE_FILE, path: dir}).then(function ({result, data, error}) {
            if (result) {
                return true
            } else {
                console.log(error);
                return false;
            }
        });
    }

    async callSocket({files, type, path}) {
        try {
            let unique = `file_${type}_${_.uniqueId()}`;
            await process.socket.emit('file', {
                files: files,
                unique: unique,
                type: type,
                path: path,
                project: project
            });
            return await new Promise(resolve => {
                let i = 1000;
                const interval = setInterval(() => {
                    if (utils.inObject(process.query_store, unique)) {
                        let rs = process.query_store[unique];
                        resolve(rs);
                        clearInterval(interval);
                    }
                    i--;
                    if (i <= 0) {
                        clearInterval(interval);
                        resolve({result: false, error: "max interval"})
                    }
                }, 2);
            });
        } catch (e) {
            console.log(e)
            return false;
        }

    }
}
const file = new File();
export default file;

