const BEPrefix = '/admin';

const constant = {
    ROUTES: {
        AUTH:{
            LOGIN: '/login'
        },
        FE: {
            HOME: '/',
            CATEGORY: '/fe/category',
            SINGLE: '/fe/single',
            CHAT: '/action/chat'
        },
        BE: {
            HOME: BEPrefix,
            CATEGORY: `${BEPrefix}/category`,
            PRODUCT: `${BEPrefix}/product`,
            BANNER: `${BEPrefix}/banner`,
            BRAND: `${BEPrefix}/brand`,
            NEWS: `${BEPrefix}/news`,
            SINGLE: `${BEPrefix}`,
            INTRO: `${BEPrefix}/intro`,
            CONTACT: `${BEPrefix}/contact`,
            SALE: `${BEPrefix}/sale`,
            FILE_MANAGER: `${BEPrefix}/file-manager`,
            USER: `${BEPrefix}/user`,
            CHAT: `${BEPrefix}/chat`,
        }
    },
    OPTIONS: {
        STATUS: [
            { key: 'o', text: '-- Select --', value: '' },
            { key: 'm', text: 'Active', value: '1' },
            { key: 'f', text: 'Inactive', value: '0' },
        ]
    },
    VAR: {
        ACTIVE: '1',
        INACTIVE: '0',
    },

}
export default constant;