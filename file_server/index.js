require('dotenv').config();

const express = require('express');
const _ = require('lodash');
const fs = require('fs');

const app = express();
let server = require('http').createServer(app);

const port = process.env.PORT || 3000;
const io = require('socket.io-client');
const socket = io(`ws://${process.env.HOST}`);

console.log(process.env.HOST, 'host ')
socket.on('connect', () => {
    console.log('connected to the socket from File server');
    const d = new Date();

});
/////
server.listen(port,  async () => {
    const d = new Date();
    console.log("Client File's started at:", d.toDateString(), ' --', d.toTimeString());
});
const DIR = '/usr/app/uploads'
socket.on('file', async function ({files, unique, type, project, path}) {
    try{
        if (!project || project !== process.env.PROJECT) {
            console.log('received from ' + project)
            return;
        }
        let rs = {result: false, error: 'on'};
        console.log(path, _.startsWith(path, '/'), "startsWith");
        switch (type) {
            case 'upload':
                rs = await writeFile(files, path);
                break
            case 'get_dir':
                rs = await getDir(path);
                break;
            case 'create_dir':
                rs = await createDir(path);
                break;
            case 'remove_dir':
                rs = await removeDir(path);
                break;
            case 'remove_file':
                rs = await removeFile(path);
                break;
        }
        await socket.emit('file_response', {...rs, unique: unique});
    }catch (error) {
        console.log('catch', error);
        socket.emit('file_response', {unique: unique, result: false, error: error});
    }
});
async function writeFile(files, p) {
    try{
        if(!files) {
            return {result: false, error: 'No file uploaded'}
        } else {
            if (_.startsWith(p, '/')) {
                p = p.substring(1)
            }
            let array = [];
            //loop all files
            await _.toArray(files).map(async (data, index) => {
                const file = await data.file.toString('base64');
                const path = p ? `${DIR}/${p}`: DIR;
                console.log(path, '##writeFile')

                await fs.writeFileSync(`${path}/${data.name}`, file, 'base64') ;
                console.log('done')
                await array.push(data.name)
            });
            return {result: true, message: 'Files are uploaded', data: array}
        }
    }catch (error) {
        console.log('catch', error);
        return {result: false, message: error.toString()}
    }
}
async function getDir(p = '') {
    try {
        let files = [];
        if (_.startsWith(p, '/')) {
            p = p.substring(1)
        }
        const path = p ? `${DIR}/${p}`: DIR;
        console.log(path, "#getDir")
        if (await fs.existsSync(path)) {
            const dir = await fs.readdirSync(path, {
                withFileTypes: true
            }).reduce((a, c) => {
                if(c.isDirectory()){
                    a.push({name:c.name, path: p})
                } else {
                    files.push(c.name)
                }
                return a
            }, []);
            console.log({dir: dir, files: files})
            return {result: true, message: 'Files', data: {dir: dir, files: files}}
        }
        return {result: true, message: 'Files', data: {dir: [], files: []}}
    } catch (error) {
        console.log('catch', error);
        return {result: false, message: error.toString()}
    }
}
async function createDir(p = '') {
    try {
        if (_.startsWith(p, '/')) {
            p = p.substring(1)
        }
        const path = p ? `${DIR}/${p}`: DIR;
        console.log(path, "#createDir");
        if (!fs.existsSync(path)) {
            await fs.mkdirSync(path)
        }
        return {result: true, message: 'Files'}
    } catch (error) {
        console.log('catch', error);
        return {result: false, message: error.toString()}
    }
}
async function removeDir(p) {
    try {
        if (p === '') {
            return {result: true, message: 'none'}
        }
        if (_.startsWith(p, '/')) {
            p = p.substring(1)
        }
        const path = `${DIR}/${p}`;
        console.log(p, path, fs.existsSync(path));
        if (fs.existsSync(path)) {
            const rs = await fs.rmdirSync(path, { recursive: true });
            console.log(rs);
        }
        return {result: true, message: 'Files'}
    } catch (error) {
        console.log('catch', error);
        return {result: false, message: error.toString()}
    }
}
async function removeFile(p) {
    try {
        if (p === '') {
            return {result: true, message: 'none'}
        }
        if (_.startsWith(p, '/')) {
            p = p.substring(1)
        }
        const path = `${DIR}/${p}`;
        console.log(path, "#removeFile", fs.existsSync(path));
        if (fs.existsSync(path)) {
            await fs.unlinkSync(path);

        }
        return {result: true, message: 'Files'}
    } catch (error) {
        console.log('catch', error);
        return {result: false, message: error.toString()}
    }
}

// Routing

// app.use(express.static(__dirname + '/public'));



